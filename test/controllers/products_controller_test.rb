require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product = products(:one)
  end

  test "should get index" do
    get products_url
    assert_response :success
  end

  test "should get new" do
    get new_product_url
    assert_response :success
  end

  test "should create product" do
    assert_difference('Product.count') do
      post products_url, params: { product: { m_image: @product.m_image, p_category: @product.p_category, p_comment: @product.p_comment, p_price: @product.p_price, p_title: @product.p_title, size_L: @product.size_L, size_M: @product.size_M, size_S: @product.size_S, size_SS: @product.size_SS, size_XL: @product.size_XL, size_XXL: @product.size_XXL } }
    end

    assert_redirected_to product_url(Product.last)
  end

  test "should show product" do
    get product_url(@product)
    assert_response :success
  end

  test "should get edit" do
    get edit_product_url(@product)
    assert_response :success
  end

  test "should update product" do
    patch product_url(@product), params: { product: { m_image: @product.m_image, p_category: @product.p_category, p_comment: @product.p_comment, p_price: @product.p_price, p_title: @product.p_title, size_L: @product.size_L, size_M: @product.size_M, size_S: @product.size_S, size_SS: @product.size_SS, size_XL: @product.size_XL, size_XXL: @product.size_XXL } }
    assert_redirected_to product_url(@product)
  end

  test "should destroy product" do
    assert_difference('Product.count', -1) do
      delete product_url(@product)
    end

    assert_redirected_to products_url
  end
end
