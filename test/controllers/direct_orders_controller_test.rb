require 'test_helper'

class DirectOrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @direct_order = direct_orders(:one)
  end

  test "should get index" do
    get direct_orders_url
    assert_response :success
  end

  test "should get new" do
    get new_direct_order_url
    assert_response :success
  end

  test "should create direct_order" do
    assert_difference('DirectOrder.count') do
      post direct_orders_url, params: { direct_order: {  } }
    end

    assert_redirected_to direct_order_url(DirectOrder.last)
  end

  test "should show direct_order" do
    get direct_order_url(@direct_order)
    assert_response :success
  end

  test "should get edit" do
    get edit_direct_order_url(@direct_order)
    assert_response :success
  end

  test "should update direct_order" do
    patch direct_order_url(@direct_order), params: { direct_order: {  } }
    assert_redirected_to direct_order_url(@direct_order)
  end

  test "should destroy direct_order" do
    assert_difference('DirectOrder.count', -1) do
      delete direct_order_url(@direct_order)
    end

    assert_redirected_to direct_orders_url
  end
end
