require 'test_helper'

class ReviewsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @review = reviews(:one)
  end

  test "should get index" do
    get reviews_url
    assert_response :success
  end

  test "should get new" do
    get new_review_url
    assert_response :success
  end

  test "should create review" do
    assert_difference('Review.count') do
      post reviews_url, params: { review: { add_comment: @review.add_comment, buy_size: @review.buy_size, comment: @review.comment, fit: @review.fit, height: @review.height, length: @review.length, m_review_id: @review.m_review_id, product_id: @review.product_id, span: @review.span, user_size: @review.user_size, weight: @review.weight } }
    end

    assert_redirected_to review_url(Review.last)
  end

  test "should show review" do
    get review_url(@review)
    assert_response :success
  end

  test "should get edit" do
    get edit_review_url(@review)
    assert_response :success
  end

  test "should update review" do
    patch review_url(@review), params: { review: { add_comment: @review.add_comment, buy_size: @review.buy_size, comment: @review.comment, fit: @review.fit, height: @review.height, length: @review.length, m_review_id: @review.m_review_id, product_id: @review.product_id, span: @review.span, user_size: @review.user_size, weight: @review.weight } }
    assert_redirected_to review_url(@review)
  end

  test "should destroy review" do
    assert_difference('Review.count', -1) do
      delete review_url(@review)
    end

    assert_redirected_to reviews_url
  end
end
