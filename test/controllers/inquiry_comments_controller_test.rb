require 'test_helper'

class InquiryCommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @inquiry_comment = inquiry_comments(:one)
  end

  test "should get index" do
    get inquiry_comments_url
    assert_response :success
  end

  test "should get new" do
    get new_inquiry_comment_url
    assert_response :success
  end

  test "should create inquiry_comment" do
    assert_difference('InquiryComment.count') do
      post inquiry_comments_url, params: { inquiry_comment: {  } }
    end

    assert_redirected_to inquiry_comment_url(InquiryComment.last)
  end

  test "should show inquiry_comment" do
    get inquiry_comment_url(@inquiry_comment)
    assert_response :success
  end

  test "should get edit" do
    get edit_inquiry_comment_url(@inquiry_comment)
    assert_response :success
  end

  test "should update inquiry_comment" do
    patch inquiry_comment_url(@inquiry_comment), params: { inquiry_comment: {  } }
    assert_redirected_to inquiry_comment_url(@inquiry_comment)
  end

  test "should destroy inquiry_comment" do
    assert_difference('InquiryComment.count', -1) do
      delete inquiry_comment_url(@inquiry_comment)
    end

    assert_redirected_to inquiry_comments_url
  end
end
