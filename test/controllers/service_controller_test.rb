require 'test_helper'

class ServiceControllerTest < ActionDispatch::IntegrationTest
  test "should get agreement" do
    get service_agreement_url
    assert_response :success
  end

  test "should get private" do
    get service_private_url
    assert_response :success
  end

end
