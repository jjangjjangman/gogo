class CreatePopups < ActiveRecord::Migration[5.0]
  def change
    create_table :popups do |t|
      t.string :image
      t.boolean :status,       default: false
      t.string :width
      t.string :height

      t.timestamps
    end
  end
end
