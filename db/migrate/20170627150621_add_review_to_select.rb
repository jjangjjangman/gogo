class AddReviewToSelect < ActiveRecord::Migration[5.0]
  def change
    add_reference :reviews, :select, foreign_key: true
  end
end
