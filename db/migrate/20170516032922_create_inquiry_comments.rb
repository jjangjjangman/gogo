class CreateInquiryComments < ActiveRecord::Migration[5.0]
  def change
    create_table :inquiry_comments do |t|
			t.belongs_to :inquiry, foreign_key: true
			t.text :title
			t.text :content

      t.timestamps
    end
  end
end
