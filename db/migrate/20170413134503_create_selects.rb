class CreateSelects < ActiveRecord::Migration[5.0]
  def change
    create_table :selects do |t|
			t.belongs_to :cart
			t.belongs_to :product
			t.belongs_to :user

			# 장바구니
			t.integer :product_quantity,			null: false, default: 1
			t.string :selected_size


      t.timestamps
    end
  end
end
