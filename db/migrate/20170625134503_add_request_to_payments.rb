class AddRequestToPayments < ActiveRecord::Migration[5.0]
  def change
	add_column :payments, :request, :text
  end
end
