class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.belongs_to :user, foreign_key: true
			t.string :title
   		t.text :postcode
      t.text :address
      t.text :address2
	
      t.timestamps
    end
  end
end
