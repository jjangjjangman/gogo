class AddPaymentToArray < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :product, :string, array: true, default:''
  end
end
