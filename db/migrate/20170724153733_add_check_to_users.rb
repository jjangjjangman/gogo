class AddCheckToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :smscheck, :boolean, default: false
    add_column :users, :emailcheck, :boolean, default: false
  end
end
