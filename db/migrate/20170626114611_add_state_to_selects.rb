class AddStateToSelects < ActiveRecord::Migration[5.0]
  def change
	add_column :selects, :refund_status, :string, default: 'not'
	rename_column :selects, :status, :exchange_status
  end
end
