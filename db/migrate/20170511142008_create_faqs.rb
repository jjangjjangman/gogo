class CreateFaqs < ActiveRecord::Migration[5.0]
  def change
    create_table :faqs do |t|
      t.string :category
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
