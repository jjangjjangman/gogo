class AddNameToPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :buyer_name, :string
    add_column :payments, :buyer_tel, :string
  end
end
