class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :p_title
      t.string :p_price
      t.text :p_comment
      t.string :m_image
      t.string :p_category
      t.boolean :size_SS,				null: false, default: 0
      t.boolean :size_S,				null: false, default: 0
      t.boolean :size_M,				null: false, default: 0
      t.boolean :size_L,				null: false, default: 0
      t.boolean :size_XL,				null: false, default: 0
      t.boolean :size_XXL,			null: false, default: 0
			t.string :image1
			t.string :image2
			t.string :image3
			t.string :image4
			t.string :image5

      t.timestamps
    end
  end
end
