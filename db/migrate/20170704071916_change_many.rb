class ChangeMany < ActiveRecord::Migration[5.0]
  def change
	remove_column :payments, :tran
	remove_column :payments, :transnum
  remove_column :exchanges, :tran
  add_reference :trans, :payment, foreign_key: true
  add_reference :trans, :exchange, foreign_key: true

  end
end
