class CreateDirectOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :direct_orders do |t|
 				t.belongs_to :user, foreign_key: true
        t.integer :total_price
				t.string :name
				t.text :postcode
				t.text :address
				t.text :address2
				t.string :number
				t.text :request

      t.timestamps
    end
  end
end
