class AddCheckToSelect < ActiveRecord::Migration[5.0]
  def change
	 add_column :selects, :check, :integer,       null: false, default: 1
  end
end
