class CreateTrans < ActiveRecord::Migration[5.0]
  def change
    create_table :trans do |t|
      t.string :transnum
		  t.string :status, default: "배송출발"

      t.timestamps
    end
  end
end
