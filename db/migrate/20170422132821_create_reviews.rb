class CreateReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :reviews do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :product, foreign_key: true
      t.string :fit
      t.string :span
      t.string :length
      t.integer :height
      t.integer :weight
      t.string :user_size
      t.string :buy_size
      t.text :comment
      t.text :add_comment

      t.timestamps
    end
  end
end
