class AddRefundToSelects < ActiveRecord::Migration[5.0]
  def change
   add_reference :selects, :refund, foreign_key: true
  end
end
