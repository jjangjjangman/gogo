class CreateExchanges < ActiveRecord::Migration[5.0]
  def change
    create_table :exchanges do |t|
        t.string :merchant_uid,       null: false, default: ""
        t.string :reason,             null: false, default: ""
        t.string :phone_number,       null: false, default: ""
				t.string :status,             default: "start"
  		  t.belongs_to :user

      t.timestamps
    end
  end
end
