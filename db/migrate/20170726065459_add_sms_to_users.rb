class AddSmsToUsers < ActiveRecord::Migration[5.0]
  def change
	rename_column :users, :smscheck, :sms
	remove_column :users, :emailcheck, :boolean
  end
end
