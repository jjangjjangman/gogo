class AddVbankToPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :vbank_num, :string
    add_column :payments, :vbank_name, :string
    add_column :payments, :vbank_holder, :string
    add_column :payments, :vbank_date, :number
  end
end
