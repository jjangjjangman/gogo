class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :questions do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :product, foreign_key: true
      t.string :q_title
      t.text :comment
			t.integer :secret,    null: false, default: "0"

      t.timestamps
    end
  end
end
