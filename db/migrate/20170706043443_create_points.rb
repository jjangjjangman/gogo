class CreatePoints < ActiveRecord::Migration[5.0]
  def change
    create_table :points do |t|

			t.string :name
			t.string :amount
			t.string :method


			t.belongs_to :user, foreign_key: true
			t.belongs_to :payment, foreign_key: true
      t.timestamps
    end
  end
end
