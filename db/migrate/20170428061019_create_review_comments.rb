class CreateReviewComments < ActiveRecord::Migration[5.0]
  def change
    create_table :review_comments do |t|
      t.belongs_to :review, foreign_key: true
      t.belongs_to :user, foreign_key: true
      t.string :rc_title
      t.text :rc_comment

      t.timestamps
    end
  end
end
