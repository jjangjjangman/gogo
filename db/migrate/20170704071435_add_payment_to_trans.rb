class AddPaymentToTrans < ActiveRecord::Migration[5.0]
  def change
	 add_reference :payments, :tran, foreign_key: true
	 add_reference :exchanges, :tran, foreign_key: true
  end
end
