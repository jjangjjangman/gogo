class ChangeTransnumToPayment < ActiveRecord::Migration[5.0]
  def change
	 remove_column :payments, :transnum
   add_column  :payments, :transnum, :string, defalt: 0
	end
end
