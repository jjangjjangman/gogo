class AddRefundToPayments < ActiveRecord::Migration[5.0]
  def change
	add_column :payments, :refund_num, :string
	add_column :payments, :refund_name, :string
	add_column :payments, :refund_bank, :string
  end
end
