class CreateQuestionImages < ActiveRecord::Migration[5.0]
  def change
    create_table :question_images do |t|
      t.string :file_id
      t.references :question, foreign_key: true

      t.timestamps
    end
  end
end
