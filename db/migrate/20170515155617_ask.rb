class Ask < ActiveRecord::Migration[5.0]
  def change
	 create_table :asks do |t|
	 t.string :title
	 t.text :content
   t.belongs_to :user, foreign_key: true

	 t.timestamps
	 end
  end
end
