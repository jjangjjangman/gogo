class AddAddressToPayments < ActiveRecord::Migration[5.0]
  def change
	add_column :payments, :buyer_addr, :text
	add_column :payments, :buyer_postcode, :string
  end
end
