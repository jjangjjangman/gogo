class CreateReviewImages < ActiveRecord::Migration[5.0]
  def change
    create_table :review_images do |t|
      t.references :review, foreign_key: true
      t.string :file_id

      t.timestamps
    end
  end
end
