class CreateQuestionComments < ActiveRecord::Migration[5.0]
  def change
    create_table :question_comments do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :question, foreign_key: true
      t.string :qc_title
      t.text :qc_comment

      t.timestamps
    end
  end
end
