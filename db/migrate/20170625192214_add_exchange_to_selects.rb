class AddExchangeToSelects < ActiveRecord::Migration[5.0]
  def change
	 add_reference :selects, :exchange, foreign_key: true
  end
end
