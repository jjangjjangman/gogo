class AddCountToFaq < ActiveRecord::Migration[5.0]
  def change
	add_column :faqs , :p_count, :integer, default: 0
	add_column :faqs , :o_count, :integer, default: 0
	add_column :faqs , :t_count, :integer, default: 0
	add_column :faqs , :e_count, :integer, default: 0
	add_column :faqs , :ex_count, :integer, default: 0
  end
end
