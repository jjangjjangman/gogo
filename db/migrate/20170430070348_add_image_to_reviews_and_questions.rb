class AddImageToReviewsAndQuestions < ActiveRecord::Migration[5.0]
  def change
		add_column :reviews, :image, :string
		add_column :questions, :image, :string
  end
end
