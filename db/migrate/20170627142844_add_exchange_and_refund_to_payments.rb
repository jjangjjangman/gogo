class AddExchangeAndRefundToPayments < ActiveRecord::Migration[5.0]
  def change
   add_reference :refunds, :payment, foreign_key: true
	 add_reference :exchanges, :payment, foreign_key: true
  end
end
