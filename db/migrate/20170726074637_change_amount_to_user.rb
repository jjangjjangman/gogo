class ChangeAmountToUser < ActiveRecord::Migration[5.0]
  def change
	change_column :users, :amount, :integer
  end
end
