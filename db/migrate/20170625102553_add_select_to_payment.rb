class AddSelectToPayment < ActiveRecord::Migration[5.0]
  def change
    add_reference :selects, :payment, foreign_key: true
  end
end
