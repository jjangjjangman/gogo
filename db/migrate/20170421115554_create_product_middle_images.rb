class CreateProductMiddleImages < ActiveRecord::Migration[5.0]
  def change
    create_table :product_middle_images do |t|
      t.string :file_id
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
