class AddColorToProducts < ActiveRecord::Migration[5.0]
  def change
	add_column :products, :colorname1, :string
	add_column :products, :colorname2, :string
	add_column :products, :colorname3, :string
  add_column :products, :colorname4, :string
	add_column :products, :colorname5, :string
  add_column :products, :color1, :string
  add_column :products, :color2, :string
  add_column :products, :color3, :string
  add_column :products, :color4, :string
  add_column :products, :color5, :string
  add_column :selects, :selected_colorname, :string
	add_column :selects, :selected_color, :string
  end
end
