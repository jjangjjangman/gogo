# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170829070708) do

  create_table "addresses", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.text     "postcode"
    t.text     "address"
    t.text     "address2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_addresses_on_user_id"
  end

  create_table "asks", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_asks_on_user_id"
  end

  create_table "carts", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_carts_on_user_id"
  end

  create_table "direct_orders", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "total_price"
    t.string   "name"
    t.text     "postcode"
    t.text     "address"
    t.text     "address2"
    t.string   "number"
    t.text     "request"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["user_id"], name: "index_direct_orders_on_user_id"
  end

  create_table "exchanges", force: :cascade do |t|
    t.string   "merchant_uid", default: "",      null: false
    t.string   "reason",       default: "",      null: false
    t.string   "phone_number", default: "",      null: false
    t.string   "status",       default: "start"
    t.integer  "user_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "payment_id"
    t.index ["payment_id"], name: "index_exchanges_on_payment_id"
    t.index ["user_id"], name: "index_exchanges_on_user_id"
  end

  create_table "faqs", force: :cascade do |t|
    t.string   "category"
    t.string   "title"
    t.text     "content"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "p_count",    default: 0
    t.integer  "o_count",    default: 0
    t.integer  "t_count",    default: 0
    t.integer  "e_count",    default: 0
    t.integer  "ex_count",   default: 0
  end

  create_table "inquiries", force: :cascade do |t|
    t.text     "title"
    t.text     "content"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_inquiries_on_user_id"
  end

  create_table "inquiry_comments", force: :cascade do |t|
    t.integer  "inquiry_id"
    t.text     "title"
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["inquiry_id"], name: "index_inquiry_comments_on_inquiry_id"
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "total_price"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "name"
    t.text     "postcode"
    t.text     "address"
    t.text     "address2"
    t.string   "number"
    t.text     "request"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "payments", force: :cascade do |t|
    t.string   "imp_uid"
    t.string   "pg_provider"
    t.integer  "amount"
    t.string   "name"
    t.string   "pay_method"
    t.boolean  "permission"
    t.boolean  "validation"
    t.string   "status"
    t.string   "receipt_url"
    t.string   "merchant_uid"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.         "addpoint"
    t.         "usepoint"
    t.text     "buyer_addr"
    t.string   "buyer_postcode"
    t.text     "request"
    t.string   "cancelrq"
    t.string   "vbank_num"
    t.string   "vbank_name"
    t.string   "vbank_holder"
    t.decimal  "vbank_date"
    t.string   "refund_num"
    t.string   "refund_name"
    t.string   "refund_bank"
    t.string   "buyer_name"
    t.string   "buyer_tel"
    t.index ["user_id"], name: "index_payments_on_user_id"
  end

  create_table "points", force: :cascade do |t|
    t.string   "name"
    t.string   "amount"
    t.string   "method"
    t.integer  "user_id"
    t.integer  "payment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["payment_id"], name: "index_points_on_payment_id"
    t.index ["user_id"], name: "index_points_on_user_id"
  end

  create_table "popups", force: :cascade do |t|
    t.string   "image"
    t.boolean  "status",     default: false
    t.string   "width"
    t.string   "height"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "product_images", force: :cascade do |t|
    t.string   "file_id"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_product_images_on_product_id"
  end

  create_table "product_middle_images", force: :cascade do |t|
    t.string   "file_id"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_product_middle_images_on_product_id"
  end

  create_table "products", force: :cascade do |t|
    t.string   "p_title"
    t.text     "p_comment"
    t.string   "m_image"
    t.string   "p_category"
    t.boolean  "size_SS",    default: false, null: false
    t.boolean  "size_S",     default: false, null: false
    t.boolean  "size_M",     default: false, null: false
    t.boolean  "size_L",     default: false, null: false
    t.boolean  "size_XL",    default: false, null: false
    t.boolean  "size_XXL",   default: false, null: false
    t.string   "image1"
    t.string   "image2"
    t.string   "image3"
    t.string   "image4"
    t.string   "image5"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.         "p_price"
    t.boolean  "status",     default: true
    t.string   "colorname1"
    t.string   "colorname2"
    t.string   "colorname3"
    t.string   "colorname4"
    t.string   "colorname5"
    t.string   "color1"
    t.string   "color2"
    t.string   "color3"
    t.string   "color4"
    t.string   "color5"
  end

  create_table "question_comments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "question_id"
    t.string   "qc_title"
    t.text     "qc_comment"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["question_id"], name: "index_question_comments_on_question_id"
    t.index ["user_id"], name: "index_question_comments_on_user_id"
  end

  create_table "question_images", force: :cascade do |t|
    t.string   "file_id"
    t.integer  "question_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["question_id"], name: "index_question_images_on_question_id"
  end

  create_table "questions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "product_id"
    t.string   "q_title"
    t.text     "comment"
    t.integer  "secret",     default: 0, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "image"
    t.index ["product_id"], name: "index_questions_on_product_id"
    t.index ["user_id"], name: "index_questions_on_user_id"
  end

  create_table "refunds", force: :cascade do |t|
    t.string   "merchant_uid", default: "",      null: false
    t.string   "reason",       default: "",      null: false
    t.string   "phone_number", default: "",      null: false
    t.string   "status",       default: "start"
    t.integer  "user_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "payment_id"
    t.index ["payment_id"], name: "index_refunds_on_payment_id"
    t.index ["user_id"], name: "index_refunds_on_user_id"
  end

  create_table "review_comments", force: :cascade do |t|
    t.integer  "review_id"
    t.integer  "user_id"
    t.string   "rc_title"
    t.text     "rc_comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["review_id"], name: "index_review_comments_on_review_id"
    t.index ["user_id"], name: "index_review_comments_on_user_id"
  end

  create_table "review_images", force: :cascade do |t|
    t.integer  "review_id"
    t.string   "file_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["review_id"], name: "index_review_images_on_review_id"
  end

  create_table "reviews", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "product_id"
    t.string   "fit"
    t.string   "span"
    t.string   "length"
    t.integer  "height"
    t.integer  "weight"
    t.string   "user_size"
    t.string   "buy_size"
    t.text     "comment"
    t.text     "add_comment"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "image"
    t.integer  "select_id"
    t.index ["product_id"], name: "index_reviews_on_product_id"
    t.index ["select_id"], name: "index_reviews_on_select_id"
    t.index ["user_id"], name: "index_reviews_on_user_id"
  end

  create_table "selects", force: :cascade do |t|
    t.integer  "cart_id"
    t.integer  "product_id"
    t.integer  "user_id"
    t.integer  "product_quantity",   default: 1,     null: false
    t.string   "selected_size"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.         "s_price"
    t.integer  "check",              default: 1,     null: false
    t.integer  "payment_id"
    t.string   "exchange_status",    default: "not"
    t.integer  "exchange_id"
    t.integer  "refund_id"
    t.string   "refund_status",      default: "not"
    t.string   "status",             default: "not"
    t.string   "selected_colorname"
    t.string   "selected_color"
    t.index ["cart_id"], name: "index_selects_on_cart_id"
    t.index ["exchange_id"], name: "index_selects_on_exchange_id"
    t.index ["payment_id"], name: "index_selects_on_payment_id"
    t.index ["product_id"], name: "index_selects_on_product_id"
    t.index ["refund_id"], name: "index_selects_on_refund_id"
    t.index ["user_id"], name: "index_selects_on_user_id"
  end

  create_table "trans", force: :cascade do |t|
    t.string   "transnum"
    t.string   "status",      default: "배송출발"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "payment_id"
    t.integer  "exchange_id"
    t.index ["exchange_id"], name: "index_trans_on_exchange_id"
    t.index ["payment_id"], name: "index_trans_on_payment_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",                   default: "",     null: false
    t.string   "email",                  default: "",     null: false
    t.string   "encrypted_password",     default: "",     null: false
    t.string   "sex"
    t.string   "phone_number"
    t.string   "birth_year"
    t.string   "birth_month"
    t.string   "birth_day"
    t.text     "postcode"
    t.text     "address"
    t.text     "address2"
    t.boolean  "admin_checked"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,      null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "bucket",                 default: 0,      null: false
    t.         "point",                  default: "2000", null: false
    t.text     "address3"
    t.text     "postcode2"
    t.text     "address4"
    t.text     "address5"
    t.text     "postcode3"
    t.text     "address6"
    t.boolean  "sms",                    default: false
    t.integer  "amount",                 default: 0
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
