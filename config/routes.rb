Rails.application.routes.draw do
  get 'payments/new'
	 resources :popups, except: [:index, :show]
  resources :direct_orders, except: [:index, :show, :update, :destory]
  resources :inquiry_comments, except: [:index]
   resources :faqs, except: [:index]
   resources :orders, except: [:index, :show, :update, :destory]
   resources :review_comments, except: [:show]
   resources :question_comments, except: [:show]
   resources :questions, except: [:index, :show]
   resources :reviews, except: [:index, :show]
   resources :selects, except: [:index]
   resources :carts, except: [:index, :edit]
	 resources :mypage, only: [:destroy, :update], as: :inquiry
	 resources :payments, only: [:new, :create]

   resources :products, except: [:index]
   devise_for :users, :controllers => {:passwords => "passwords", :registrations => "registrations", :sessions => "sessions", :confirmaitions => "confirmations"}
   root 'home#category'
   resources :home, except: [:index, :create, :new, :destroy, :update]

	#get "/home" => 'home#category'
	get "/all" => 'home#index'
	get "/compare" => 'home#compare'

	post "/nonmember" => 'mypage#nonmember'
	get "/nonmember" => 'mypage#nonmember'
	get "/nonmember/refund/:id" => 'mypage#refund'
	post "/nonmember/refund/:id" => 'mypage#nonmember_refund'
	#고객행복센터	
	get "/happy" => 'home#happy' #FAQ
	get "/happy/FAQ" => 'home#happy' #FAQ
	get "/happy/shopping" => 'home#shopping' #이용안내
	get "/happy/refund" => 'home#refund' #반품/교환 안내
	#카테고리
	get "/category/:id" 	=> 'category#show'
  #결 제
	 get "/payments/complete" => 'payments#complete'
	 post "/payments/new"
   post "/orders/new" => 'orders#new'
	 get "/payments/refund/:id" => 'payments#refund'
	 get "/nonmember_direct_orders" =>'direct_orders#nonmember'
	 post "/nonmember_orders" => 'orders#nonmember'
	 get "/nonmember_orders" => 'orders#nonmember'
	 get "/nonmember_direct_orders" => 'direct_orders#nonmember'
	#환 불
	 post "/payments/refund" 
	#리 뷰
	 get "/reviews/new/:product_id" => 'reviews#new'
	 get "/review_comments/new/:review_id" => 'review_comments#new'
	#상품 질문
	 get "/questions/new/:product_id" => 'questions#new'
	 get "/question_comments/new/:question_id" => 'question_comments#new'
	#일대일 문의
	 get "/inquiry_comments/new/:inquiry_id" => 'inquiry_comments#new'
	 #장바구니
	 get "/cart/empty" => 'mypage#empty'
   #상품 페이지
   get "/products/:p_category/:id" => 'products#show' 
   #마이페이지
	 get "/mypage/purchase_statement"
   get "/mypage/exchange_return"  
   get "/mypage/my_activity"
   get "/mypage/user_information"
	 post "/mypage/user_information/edit" => 'mypage#user_information_edit'
   get "/mypage/FAQ"
	 get "/mypage" => 'mypage#purchase_statement'
	 post "mypage/FAQ" => 'mypage#create'
	 get "/mypage/:id" => 'mypage#edit'
	 delete "/mypage/FAQ/:id" => 'mypage#destroy'
	 get "/mypage/purchase_statement/:id" => 'mypage#order_info' #주문정보
	 get "/mypage/exchange_question/:id" => 'mypage#exchange_question' #교환문의
	 post "/mypage/exchange_question/:id/" => 'mypage#exchange_create'
	 get "/mypage/cancel/:id" => 'mypage#cancel_request'
	 get "/mypage/point/details" => 'mypage#point'
	 post "/mypage/refund_vbank" 

   #관리자 페이지
	 post "/admin/paymentstatus"
   get "/admin" => 'admin#index'
	 get "/admin/product_upload" => 'products#new'
	 get "/admin/product_management"
	 get "/admin/product_management/:id" => 'admin#soldout'
	 get "/admin/customer_db"
	 get "/admin/order_delivery"
	 get "/admin/order_deposit"
	 get "/admin/operation_review"
	 get "/admin/operation_alram"
	 get "/admin/operation_question"
	 get "/admin/operation_faq"
	 get "/admin/operation_inquiry"
	 get "/admin/exchange_question"
	 post "/admin/exchange_question" => 'admin#exchange_complete'
	 get "/admin/refund_question"
	 post "/admin/refund_question" => 'admin#refund_complete'
	 get "/admin/payments"
	 get "/admin/order_delivery/:id" => 'admin#transnum'
	 get "/admin/point" => 'admin#point'
	 post "/admin/point" => 'admin#point_create'
	 get "/admin/point_info" => 'admin#point_info'
	 get "/admin/customer_db/:id" => 'admin#customer_info'
   get "/admin/manual" => 'admin#manual'
	 get "/admin/popup" => 'admin#popup'
	 get "/admin/popup/true" => 'admin#popup_true'
	 get "/admin/popup/false" => 'admin#popup_false'
	 get "/admin/sale"
	 # 서비스 페이지(회사소개, 이용안내 ..)
	 get 'service/agreement'
	 get 'service/about'
	 get 'service/private'
	 get 'service/guide'
   # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
