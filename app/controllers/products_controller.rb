class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_admin1, only: [:new, :create, :update]
	before_action :authenticate_admin, only: [:new, :create, :update]
	before_action :count, only: [:new, :edit]

  # GET /products
  # GET /products.json
  def index
    @products = Product.all
  end

  # GET /products/1
  # GET /products/1.json
  def show
		@review =  Review.all.order('created_at desc')
		@question = Question.all.order('created_at desc')
		if user_signed_in?
  	@payment = current_user.payments
		@payment.each do |payment|
		 payment.selects.each do |select|
		    if select.product_id.to_i == @product.id.to_i
				    @check = true
			  end
			end
		end
		else
			@check = false
		end
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
 	 @product.size_SS = false
   @product.size_S = false
	 @product.size_M = false
	 @product.size_L = false
	 @product.size_XL = false
	 @product.size_XXL = false
	 @product.status = true
	 selects = @product.selects
   selects.each do |select|
    if select.product.status != false && select.payment_id.blank? && select.status == 'soldout'
       select.status = 'not'
       select.save
    end
   end

    respond_to do |format|
      if @product.update(product_params)
        format.html { 
					flash[:msg] = '상품등록이 완료되었습니다.'
					redirect_to @product 
					}
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to admin_product_management_path, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

	def count
   @payments_count = Payment.where(:created_at => (Time.now - 1.day)..Time.now, :status => 'paid')
   @trans_count = Tran.where.not('status == "배송출발" OR status == "배달완료"')

   @exchanges_count = Exchange.where(:created_at => (Time.now - 1.day)..Time.now, :status => 'start')
   @refunds_count = Refund.where(:created_at => (Time.now - 1.day)..Time.now, :status => 'start')

   @reviews_count = Review.where(:created_at => (Time.now - 1.day)..Time.now)
   @questions_count = Question.where(:created_at => (Time.now - 1.day)..Time.now)
   @inquirys_count = Inquiry.where(:created_at => (Time.now - 1.day)..Time.now)
	end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:p_title, :p_price, :p_comment, :m_image, :image1, :image2, :image3, :image4, :image5, :p_category, :colorname1, :colorname2, :colorname3, :colorname4, :colorname5, :color1, :color2, :color3, :color4, :color5, :size_SS, :size_S, :size_M, :size_L, :size_XL, :size_XXL, product_images_files: [], product_middle_images_files: [])
    end
end
