class QuestionsController < ApplicationController
  before_action :set_question, only: [:show, :edit, :update, :destroy]
	before_action :authenticate, except: [:show]	

  # GET /questions
  # GET /questions.json
  def index
    @questions = Question.all
  end

  # GET /questions/1
  # GET /questions/1.json
  def show
	 if @question.secret == 1
		if current_user != @question.user && current_user.admin_checked != true
	     respond_to do |format|
		    format.html { redirect_to '/', notice: '잘못된 접근입니다..' }
			end
		end		 
   end
	end

  # GET /questions/new
  def new
    @question = Question.new
		 @product = Product.find(params[:product_id])
		 session[:product_id] = params[:product_id]
		 @user = current_user
  end

  # GET /questions/1/edit
  def edit
		@question = Question.find(params[:id])
    @product = Product.find(@question.product_id)

    if @question.user_id != current_user.id && current_user.id != 1
       redirect_to @product
    end
  end

  # POST /questions
  # POST /questions.json
  def create
    @question = Question.new(question_params)
 			@user = current_user
      @question.product_id = session[:product_id]
	    @question.user_id = @user.id
			@product = Product.find(session[:product_id])


    respond_to do |format|
      if @question.save
        format.html { redirect_to @product, notice: 'Question was successfully created.' }
        format.json { render :show, status: :created, location: @question }
				session[:product_id] = nil
      else
				format.html {
        flash[:question] = '필수 사항을 모두 입력해주세요!'
	        redirect_to :back
	        }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /questions/1
  # PATCH/PUT /questions/1.json
  def update
	@product = @question.product
    respond_to do |format|
      if @question.update(question_params)
        format.html { redirect_to @product, notice: 'Question was successfully updated.' }
        format.json { render :show, status: :ok, location: @question }
      else
        format.html {
			  flash[:question] = '필수 사항을 모두 입력해주세요!'
			    redirect_to :back
				  }
				format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    @question.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Question was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question
      @question = Question.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_params
      params.require(:question).permit(:user_id, :product_id, :q_title, :comment, :secret, :image, question_images_files: [])
    end
end
