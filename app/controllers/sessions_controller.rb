class SessionsController < Devise::SessionsController
  def new
     flash[:notice1] = "이메일을 확인해주세요!" if params['failure_reason'] == :not_found_in_database
	 flash[:notice1] = "이메일 인증을 진행해 주세요!" if params['failure_reason'] == 'unconfirmed'
	 flash[:notice1] = "비밀번호를 확인해주세요!" if params['failure_reason'] == :invalid
    self.resource = resource_class.new(sign_in_params)
    clean_up_passwords(resource)
    yield resource if block_given?
    respond_with(resource, serialize_options(resource))
  end

  def create
    self.resource = warden.authenticate!(auth_options)
    sign_in(resource_name, resource)  
    yield resource if block_given?
    respond_with resource, location: after_sign_in_path_for(resource)
  end
end
