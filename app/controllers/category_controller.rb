class CategoryController < ApplicationController
	before_action :category_set
	
	def show
	end
	
	private
	
	def category_set
		 @category = params[:id]
		 @product_true = Product.where(p_category: params[:id], status: true).order('created_at desc')
	     @product_false = Product.where(p_category: params[:id], status: false).order('created_at desc')
	     @product = @product_true + @product_false
	end
end