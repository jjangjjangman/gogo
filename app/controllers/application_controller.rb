class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
	before_action :current_cart, if: :user_signed_in?
	before_action :admincheck
	after_filter :store_location
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:sex])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:phone_number])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:birth_year])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:birth_month])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:birth_day])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:postcode])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:address])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:address2])
		devise_parameter_sanitizer.permit(:sign_up, keys: [:postcode2])
		devise_parameter_sanitizer.permit(:sign_up, keys: [:address3])
		devise_parameter_sanitizer.permit(:sign_up, keys: [:address4])
    devise_parameter_sanitizer.permit(:account_update, keys: [:sex])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:phone_number])
    devise_parameter_sanitizer.permit(:account_update, keys: [:birth_year])
    devise_parameter_sanitizer.permit(:account_update, keys: [:birth_month])
    devise_parameter_sanitizer.permit(:account_update, keys: [:birth_day])
    devise_parameter_sanitizer.permit(:account_update, keys: [:postcode])
    devise_parameter_sanitizer.permit(:account_update, keys: [:address])
    devise_parameter_sanitizer.permit(:account_update, keys: [:address2])
		devise_parameter_sanitizer.permit(:account_update, keys: [:address3])
		devise_parameter_sanitizer.permit(:account_update, keys: [:address4])
		devise_parameter_sanitizer.permit(:account_update, keys: [:postcode2])
		devise_parameter_sanitizer.permit(:account_update, keys: [:sms])
	end

	def store_location
  # store last url as long as it isn't a /users path
  session[:previous_url] = request.fullpath unless request.fullpath =~ /\/users/
	end

	def after_sign_in_path_for(resource)
  session[:previous_url] || root_path
	end

	def authenticate
	  redirect_to user_session_path unless user_signed_in?
	end

	def authenticated
		redirect_to ':back' unless user_signed_in?
	end
	
	def admincheck
		if user_signed_in?
			if current_user.id == 1
				current_user.admin_checked = true
				current_user.save
			end
		end
	end

	def authenticate_admin           ##관리자 페이지 (로그인 후에 막기)
	  redirect_to '/' unless current_user.admin_checked
  end

	def authenticate_admin1           ##관리자 페이지 (로그인 전에 막기)
	  redirect_to '/' unless user_signed_in?
	end

	private

		def current_cart
				if user_signed_in?
					  Cart.find(current_user.cart)
				else
					  Cart.find(session[:cart_id])
				end
        rescue ActiveRecord::RecordNotFound
         cart = Cart.create
					if user_signed_in?
         cart.user_id = current_user.id
         cart.save
         current_user.update(bucket: cart.id)
          else
					session[:cart_id] = cart.id
				 end
				 cart
		end
end
