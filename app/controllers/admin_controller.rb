class AdminController < ApplicationController
 before_action :authenticate_admin1
 before_action :authenticate_admin
 before_action :count, except: [:soldout, :exchange_complete, :point_create, :popup_true, :popup_false]
	
 def index
 end

 def soldout
  product = Product.find_by_id(params[:id])
 	product.status = false
	product.size_SS = false
	product.size_S = false
	product.size_M = false
	product.size_L = false
	product.size_XL = false
	product.size_XXL = false
	product.save
	selects = product.selects
	selects.each do |select|
    if select.product.status != true && select.payment_id.blank? && select.status == 'not'	 
       select.status = 'soldout'
			 select.save
	  end
	 end
	redirect_to :back
 end

 def payments
    @payments = Payment.all.order('created_at desc')
		@payments.each do |payment|
		iamport =Iamport.find(payment.merchant_uid).parsed_response['response']
		payment.status = iamport['status']
		payment.selects.each do |select|
		select.status = 'paid' if payment.status == 'paid'
		select.save
		end
		payment.save
		end
  if params[:merchant_uid]
    @payments= Payment.search(params[:merchant_uid]).order("created_at desc")
  else
    @payments = Payment.all.order("created_at desc")
  end
 end

 def product_management
 		@product = Product.all
		if params[:search]
			@product = Product.search(params[:search])
		else
			@product = Product.all
		end
		 respond_to do |format|
  	 format.html
  	 format.csv {  send_data @product.to_csv.force_encoding("UTF-8").encode("EUC-KR") }
  	 format.xls #{ send_data @product.to_csv.force_encoding("UTF-8").encode("EUC-KR").(col_sep: "\t") }
  end
 end

 def customer_db
 	@user = User.all.order('created_at desc')
	if params[:name]
    @user = User.search(params[:name]).order("created_at desc")
  else
    @user = User.all.order("created_at desc")
	end
	respond_to do |format|
	format.html
	format.csv {  send_data @user.to_csv.force_encoding("UTF-8").encode("EUC-KR") }
	format.xls #{ send_data @user.to_csv.force_encoding("UTF-8").encode("EUC-KR").(col_sep: "\t") } 
  end
 end

 def customer_info
	@user = User.find(params[:id])
	@points = @user.points
 end
	
 def transnum
  if params[:delivery]
		 payment = Payment.find_by_merchant_uid(params[:id])
		 payment.tran.status = params[:delivery]
		 redirect_to :back if payment.tran.save
  else
   tran = Tran.new
   payment = Payment.find_by_imp_uid(params[:id])
   tran.transnum = params[:transnum]
	 tran.payment_id = payment.id
	 redirect_to :back if tran.save
  end
 end

 def order_delivery
    @payments = Payment.all.order('created_at desc')
		@payment = @payments.search('paid').order("created_at desc") #Payment-> payments
		 respond_to do |format|
 		 format.html
  	 format.csv {  send_data @payments.to_csv.force_encoding("UTF-8").encode("EUC-KR") }
  	 format.xls #{ send_data @payments.to_csv.force_encoding("UTF-8").encode("EUC-KR").(col_sep: "\t") }
  	end
 end

 def manual
 end

 def operation_review
      @review = Review.all.order('created_at desc')
 end

 def operation_question
    	@question = Question.all.order('created_at desc')
 end

 def operation_alram
 end

 def operation_faq
      @faq = Faq.all.order('created_at desc')
 end

 def operation_inquiry
 			@inquiry = Inquiry.all.order('created_at desc')
 end

 def exchange_question
      @exchanges = Exchange.all.order('created_at desc')
 end

 def exchange_complete
      @select = Select.find_by_id(params[:id])
			exchange = @select.exchange
			@select.exchange_status = "exchange_complete"
			exchange.status = "complete"
			@select.save
			exchange.save
			redirect_to :back
 end

 def refund_question
    @refunds = Refund.all.order('created_at desc')
 end

 def point
  @point = Point.new
 end

 def point_create
  payment = Payment.find_by_merchant_uid(params[:payment_merchant_uid])
	user = User.find_by_email(params[:user_email])
  @point = Point.new(name:params[:name], amount:params[:amount], method:params[:method],payment_id:payment.id, user_id:user.id)
		if @point.method == 'use' 
		user.point = user.point.to_i - @point.amount.to_i
		user.save
		@point.save
		  redirect_to :back
		elsif @point.method == 'add' 
		user.point = user.point.to_i + @point.amount.to_i
		user.save
		@point.save
		    redirect_to :back
		else
			redirect_to :back
		end
 end

 def point_info
  @point = Point.all.order('created_at desc')
	  if params[:name]
    @user = User.search(params[:name]).order("created_at desc")
		@user.each do |user|
		@point = Point.where('user_id LIKE ?', "#{user.id}%").order("created_at desc")
		end
   end
 end

 def refund_complete
     @select = Select.find_by_id(params[:id])
		 	refund = @select.refund
			refund.status = "complete"
      @select.refund_status = "refund_complete"
      @select.save
			refund.save
      redirect_to :back 
 end

 def popup
  @popup = Popup.first
 end

 def popup_true
  popup = Popup.first
	popup.status = true
	redirect_to '/admin/popup' if popup.save
 end

 def popup_false
   popup = Popup.first
	 popup.status = false
	 redirect_to '/admin/popup' if popup.save
 end

 def sale
 @products = Product.all
 @payments = Payment.all
 @all = 0, @paid = 0, @ready = 0, @cancelled = 0 
 @payments.each do |payment|
			payment.selects.each do |select|
			@paid += select.product_quantity * select.s_price if payment.status == "paid"
			@ready += select.product_quantity * select.s_price if payment.status == "ready"
			@cancelled += select.product_quantity * select.s_price if payment.status == "cancelled"
  end
 end
 end

 def count
	 @payments_count = Payment.where(:created_at => (Time.now - 1.day)..Time.now, :status => 'paid')
   @trans_count = Tran.where.not('status == "배송출발" OR status == "배달완료"')
  
	@exchanges_count = Exchange.where(:created_at => (Time.now - 1.day)..Time.now, :status => 'start')
   @refunds_count = Refund.where(:created_at => (Time.now - 1.day)..Time.now, :status => 'start')
  
	 @reviews_count = Review.where(:created_at => (Time.now - 1.day)..Time.now)
   @questions_count = Question.where(:created_at => (Time.now - 1.day)..Time.now)
   @inquirys_count = Inquiry.where(:created_at => (Time.now - 1.day)..Time.now)
 end

end
