class ReviewsController < ApplicationController
  before_action :set_review, only: [:show, :edit, :update, :destroy]
	before_action :authenticate, except: [:show]

  # GET /reviews
  # GET /reviews.json
  def index
    @reviews = Review.all
  end
  # GET /reviews/1
  # GET /reviews/1.json
  def show
  end
  # GET /reviews/new
  def new
		@review = Review.new
	 	@product = Product.find(params[:product_id])
		session[:product_id] = params[:product_id]
		@user = current_user
  end
  # GET /reviews/1/edit
  def edit
	  @review = Review.find(params[:id])
		@product = Product.find(@review.product_id)
		
		if @review.user_id != current_user.id && current_user.id != 1
		   redirect_to @product
		end

  end
  # POST /reviews
  # POST /reviews.json
  def create
	 @review = Review.new(review_params)
   @review.user_id = current_user.id
	 @review.product_id = session[:product_id]
	 @product = Product.find(session[:product_id])
	 @payment = current_user.payments
   @payment.each do |payment|
    payment.selects.each do |select|
     if select.product_id.to_i == @product.id.to_i
         @review.select_id = select.id
      end
		 end
   end
    
		respond_to do |format|
      if @review.save
        format.html { redirect_to @product, notice: 'Review was successfully created.' }
        format.json { render :show, status: :created, location: @review }
				session[:product_id] = nil
      else
        format.html {
					flash[:review] = '필수 사항을 모두 입력해주세요!' 
					redirect_to :back 
					}
        format.json { render json: @review.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reviews/1
  # PATCH/PUT /reviews/1.json
  def update
	@product = @review.product
	   respond_to do |format|
      if @review.update(review_params)
        format.html { redirect_to @product , notice: 'Review was successfully updated.' }
        format.json { render :show, status: :ok, location: @review }
      else
    		format.html {
            flash[:review] = '필수 사항을 모두 입력해주세요!'
            redirect_to :back
            }
        format.json { render json: @review.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reviews/1
  # DELETE /reviews/1.json
  def destroy
	@product = @review.product
    @review.destroy
    respond_to do |format|
      format.html { redirect_to @product, notice: 'Review was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_review
      @review = Review.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def review_params
      params.require(:review).permit(:user_id, :product_id, :fit, :span, :length, :height, :weight, :user_size, :buy_size, :comment, :add_comment, :image, review_images_files: [])
    end
end
