class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy], except: [:confrim] 
	before_action :authenticate, except: [:nonmember]


  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

	def confirm
		 @order = Order.new
		 @user = current_user
		 @name = params[:name]
		 @number = params[:number]
		 @request = params[:request]
		 @product_price = params[:product_price]
		 @cart = current_cart
		 select = @cart.selects.order('created_at desc')
     @select = select.where.not(status: 'soldout')
		 @addpoint = params[:addpoint]

		 if @name == ""
		 @name = @user.name.to_s
		 end

		 if @number == "" && @user.phone_number == ""
       respond_to do |format|
        format.html {
            flash[:number] = '연락처를 입력해주세요!'
            redirect_to :back
            return
            }
       end
     elsif @number == ""
     @number = @user.phone_number
     end

		 if @request == ""
		 @request = "요청사항이 없습니다!"
		 end

	   delivery = params[:delivery]
     if delivery == "1"
       postcode = params[:postcode]
       if postcode == ""
        @order.postcode = @user.postcode
        @order.address = @user.address
        @order.address2 = @user.address2
       else
        @order.postcode = params[:postcode]
        @order.address = params[:address]
        @order.address2 = params[:address2]
       end
     elsif delivery == "2"
       postcode = params[:postcode2]
       if postcode == ""
        @order.postcode = @user.postcode2
        @order.address = @user.address3
        @order.address2 = @user.address4
       else
        @order.postcode = params[:postcode2]
        @order.address = params[:address3]
        @order.address2 = params[:address4]
       end
     elsif delivery == "3"
       postcode = params[:postcode3]
       if postcode == ""
        @order.postcode = @user.postcode3
        @order.address = @user.address5
        @order.address2 = @user.address6
       else
        @order.postcode = params[:postcode3]
        @order.address = params[:address5]
        @order.address2 = params[:address6]
       end
     else
       @order.postcode = params[:postcode4]
       @order.address = params[:address7]
       @order.address2 = params[:address8]
     end

  	 if @order.postcode == ""
          respond_to do |format|
         format.html {
            flash[:address] = '주소를 입력해주세요!'
            redirect_to :back
            return
            }
         end
      end
      
		if params[:check] == '1'
			 if @user.point.to_i <= @product_price.to_i
			    @usepoint = @user.point.to_i
			 else
          @usepoint = @product_price.to_i
			 end
		else
			 @usepoint = 0
		end

	end

  # GET /orders/new
  def new
		 @cart = current_cart
     @order = Order.new
		 @user = current_user
		 select = @cart.selects.order('created_at desc')
     @select = select.where.not(status: 'soldout')
  end

	def nonmember	
	  @cart = current_cart
		select = @cart.selects.order('created_at desc')
		@select = select.where.not(status: 'soldoout')
	end

  # GET /orders/1/edit
  def edit
  end

	def complete
	end

  # POST /orders
  # POST /orders.json
  def create
	  @cart = current_cart
		name = params[:name]
		postcode = params[:postcode]
		address = params[:address]
		address2 = params[:address2]
		number = params[:number]
		request = params[:request]
		ordercheck = params[:ordercheck]
		point = params[:point]
		total_price= params[:total_price]
		user_id = params[:user_id]
		if ordercheck == '1' 
    	@order = Order.new(name: name,postcode: postcode, address: address, address2: address2,number: number,request: request,total_price: total_price,user_id: user_id)

    	@order = 
    	respond_to do |format|
      	if @order.save
					current_user.point = point
					current_user.save
					@cart.selects.each do |f|
					f.order_id = @order.id
					f.save
					@cart.destroy
					end
        	format.html { redirect_to @order, notice: 'Order was successfully created.' }
        	format.json { render :show, status: :created, location: @order }
      	else
        	format.html { render :new }
        	format.json { render json: @order.errors, status: :unprocessable_entity }
      	end
			end
		else
         respond_to do |format|
          format.html {
            flash[:ordercheck] = '결제정보확인을 체크해 주세요.'
            redirect_to :back
          }
         format.json { render json: @review.errors, status: :unprocessable_entity }
       end
		end
	end	

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
 	  def order_params
        params.require(:order).permit(:user_id, :totatl_price, :name, :postcode, :address, :address2, :number, :request)
    end
end
