require 'nokogiri'
require 'open-uri'

class MypageController < ApplicationController
	before_action :set_inquiry, only: [:edit, :update, :destroy]
	before_action :authenticate, except: [:nonmember, :nonmember_refund, :refund]
	before_action :set_payment, only: [:exchange_question, :order_info]
	before_action :check_refund, only: [:exchange_question]
	
	def refund 
		@payment = Payment.find(params[:id])
	end
	
	def nonmember
	 if params[:merchant_uid] == "" || params[:buyer_tel] == "" || params[:buyer_name] == ""
		 @payment = nil
		 if  params[:merchant_uid] && params[:buyer_tel] && params[:buyer_name]
			  flash[:payment] = "3가지 모두 입력해주세요"
		 elsif (params[:merchant_uid] == "" && params[:buyer_tel] == "") || ( params[:buyer_name] == "" && params[:buyer_tel] == "") || ( params[:merchant_uid] == "" && params[:buyer_name] == "")
			  flash[:payment] = "나머지 사항을 입력해 주세요"
		 elsif params[:merchant_uid] == ""
		 		flash[:payment] = "주문번호를 입력해주세요"
		 elsif params[:buyer_tel] == ""
		  	flash[:payment] = "연락처를 입력해주세요"
		 elsif params[:buyer_name] == ""
		 		flash[:payment] = "성함을 입력해주세요"
		 end
   elsif params[:merchant_uid] && params[:buyer_tel] && params[:buyer_name]
	 	#@payment = Payment.search_non(params[:merchant_uid],params[:buyer_tel],params[:buyer_name]).order("created_at desc")
			@payment = Payment.find_by(merchant_uid: "merchant_" + params[:merchant_uid], buyer_tel: params[:buyer_tel], buyer_name: params[:buyer_name])
			unless @payment
					flash[:payment] = "일차하는 주문정보가 없습니다."
			end
	 else
		 @payment = nil

	 end
	end
	
	def nonmember_refund
    if params['check'] && params['reason'] != "" && params['category'] != ""
      if params['category'] == "exchange"
    @exchange = Exchange.new
    @exchange.merchant_uid = params[:merchant_uid]
    @exchange.reason = params[:reason]
    @exchange.phone_number = params[:phone_number]
    @exchange.status = "request"
    @exchange.payment_id = params[:payment_id]
    @exchange.save

    @check= params['check']
      i=0
     while i < @check.size
      @select = Select.find_by_id(@check[i.to_i])
      @select.exchange_id = @exchange.id
      @select.exchange_status = "exchange_request"
      @select.save
      i += 1
     end
      redirect_to '/nonmember'
      elsif params['category'] == "refund"
      @refund = Refund.new
    @refund.merchant_uid = params[:merchant_uid]
    @refund.reason = params[:reason]
    @refund.phone_number = params[:phone_number]
    @refund.status = "request"
    @refund.payment_id = params[:payment_id]
    @refund.save

    @check= params['check']
      i=0
     while i < @check.size
      @select = Select.find_by_id(@check[i.to_i])
      @select.refund_id = @refund.id
      @select.refund_status = "refund_request"
      @select.save
      i += 1
     end
      redirect_to '/nonmember'
      end
    else
      flash[:alert] = "필수사항(상품 및 카테고리 선택, 사유 작성)을 확인해주세요."
      redirect_to :back
    end
  end


	def cancel_request
   payment = Payment.find_by_imp_uid(params[:id])
	 payment.cancelrq = "request"
	 payment.save
	 redirect_to :back
	end

	def exchange_question
	end

	def exchange_create
  

	  if params['check'] && params['reason'] != "" && params['category'] != ""	
			if params['category'] == "exchange"
    @exchange = Exchange.new
    @exchange.merchant_uid = params[:merchant_uid]
    @exchange.reason = params[:reason]
    @exchange.phone_number = params[:phone_number]
    @exchange.status = "request"
		@exchange.user_id = current_user.id
		@exchange.payment_id = params[:payment_id]
    @exchange.save

	  @check= params['check']
		  i=0
     while i < @check.size
      @select = Select.find_by_id(@check[i.to_i])
      @select.exchange_id = @exchange.id
			@select.exchange_status = "exchange_request"
      @select.save
      i += 1
		 end	
			redirect_to '/mypage/exchange_return'
			elsif params['category'] == "refund"
      @refund = Refund.new
    @refund.merchant_uid = params[:merchant_uid]
    @refund.reason = params[:reason]
    @refund.phone_number = params[:phone_number]
    @refund.status = "request"
		@refund.user_id = current_user.id
		@refund.payment_id = params[:payment_id]
    @refund.save

    @check= params['check']
      i=0
     while i < @check.size
      @select = Select.find_by_id(@check[i.to_i])
      @select.refund_id = @refund.id
      @select.refund_status = "refund_request"
      @select.save
      i += 1
     end
      redirect_to '/mypage/exchange_return'
			end
		else
			flash[:alert] = "필수사항(상품 및 카테고리 선택, 사유 작성)을 확인해주세요."
			redirect_to :back
		end
	end


	def order_info #구매 내역-> 주문정보
	end

	def purchase_statement #구매 내역
	@user = current_user
	@payment = @user.payments.order('created_at desc')
	  @payment.each do |payment|
    iamport =Iamport.find(payment.merchant_uid).parsed_response['response']
    payment.status = iamport['status']
		if payment.status == "paid" && payment.pay_method == "vbank"
			  if payment.permission != true
					@user.amount = @user.amount + payment.amount
					 	num = 0
					 		payment.selects.each do |select|
					 			num += select.product_quantity * select.s_price
					 		end
           payment.addpoint  = (((num.to_i * 0.02) + 5 ).to_i / 10 ) * 10
          # p_point1  = p_point + 5
          # p_point2  = p_point1.to_i / 10 
          # payment.addpoint  = p_point2 * 10
					 		if payment.addpoint != 0
					 				point = Point.new(name: '상품구매로 포인트 적립', amount: payment.addpoint, method: 'add', payment_id: payment.id, user_id: current_user.id)
       		 				point.save
					 		end
					 @user.point = @user.point.to_i + payment.addpoint.to_i
					 @user.save
				end
				payment.permission = true
		end
    payment.save
	  if payment.tran
			 tran = payment.tran
      url1 = "http://nplus.doortodoor.co.kr/web/detail.jsp?slipno="
      url2 = tran.transnum
		 @url = url1 +url2
			data = Nokogiri::HTML(open(@url),nil,'euc-kr')
			 data.xpath("//b").each do |notice|
			      @abc = notice.inner_text
			    if @abc[0..5] == '운송장 번호'
						tran.status = @abc[23..26]
					  tran.save
					end
			end	  
		 end
		end
  end

	def refund_vbank #가상계좌 환불 입력받기
	payments = current_user.payments
	payment = payments.find_by_merchant_uid(params[:id])
	payment.refund_num = params[:refund_num]
	payment.refund_name = params[:refund_name]
	payment.refund_bank = params[:refund_bank]

	redirect_to :back if payment.save
	end

	def point
	  @point = current_user.points.order('created_at desc')
	end

  def exchange_return #교환/반품
	@user = current_user
	@exchange = current_user.exchanges.order('created_at desc')
	@refund = current_user.refunds.order('created_at desc')
  end

  def my_activity #나의 활동
	@user = current_user
	@payment = @user.payments
	@review =  Review.all.order('created_at desc')
	@question = Question.all.order('created_at desc')

  end

  def user_information #회원정보
  end

	def user_information_edit
		user = current_user
	  if user.valid_password?(params[:password])
			authenticity_token = params[:authenticity_token]
			redirect_to controller: 'registrations', action: 'edit', option: authenticity_token
			else
	 		 flash[:alert] = '비밀번호가 일치하지 않습니다.'
  		 redirect_to '/mypage/user_information'
		end
	end
  
  def FAQ #FAQ
	 @faq = Faq.all.order('created_at desc')
	 @user = current_user
	 @inquiry = Inquiry.all.order('created_at desc')
  end

	def create
	 title = params[:title]
	 content = params[:content]
	 user_id = current_user.id
	 @inquiry = Inquiry.new(title: title,content: content,user_id: user_id)

	 respond_to do |format|
	 	if @inquiry.save
				format.html {redirect_to '/mypage/FAQ' }
		else
			format.html {
						flash[:inquiry] = '제목과 내용을 입력해주세요!'
						redirect_to :back
			}
		end
	 end
	end

	def edit
		@inquiry = Inquiry.find(params[:id])

    if @inquiry.user_id != current_user.id && current_user.id != 1
       redirect_to '/mypage/FAQ'
    end
	end

	def update
		title = params[:title]
		content = params[:content]

    respond_to do |format|
      if @inquiry.update(inquiry_params)
        format.html {redirect_to '/mypage/FAQ'}
      else
        format.html {
					flash[:inquiry] = '제목과 내용을 입력해주세요!'
          redirect_to :back
				}
      end
    end
  end
	
  def destroy
      @inquiry.destroy
      respond_to do |format|
        format.html { redirect_to '/mypage/FAQ'}
      end
  end

	def inquire
	 @user = current_user
	 InquiryMailer.inquire_email(@user).deliver_later
	 redirect_to :back
	end


	def empty #장바구니 빈페이지
	end

	 private
   # Use callbacks to share common setup or constraints between actions.
   def set_inquiry
      @inquiry = Inquiry.find(params[:id])
   end
	 def inquiry_params
		 params.require(:inquiry).permit(:user_id,:title, :content)
	 end

	 def set_payment
	   @payment = Payment.find_by_id(params[:id])
	 end

	 def check_refund
	  payment = Payment.find(params[:id])
		if payment.status == "paid"
	     if payment.tran 
        if payment.tran.status != "배달완료"
				 redirect_to  '/mypage/purchase_statement'
				end
 			 else
				 redirect_to  '/mypage/purchase_statement'
			end
	  elsif payment.status == "cancelled"
		  if payment.cancelrq == "request" || payment.cancelrq =="complete"
				redirect_to  '/mypage/purchase_statement'
			end
		else
			redirect_to  '/mypage/purchase_statement'
		end
	 end
end
