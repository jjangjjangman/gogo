class SelectsController < ApplicationController
  before_action :set_select, only: [:show, :edit, :update, :destroy]

  # GET /selects
  # GET /selects.json
  def index
    @selects = Select.all
  end

  # GET /selects/1
  # GET /selects/1.json
  def show
  end

  # GET /selects/new
  def new
    @select = Select.new
  end

  # GET /selects/1/edit
  def edit
  end

  # POST /selects
  # POST /selects.json
  def create
	selected_color = params['selected_color']
  selected_colorname = params['selected_colorname'].map(&:to_s)
	selected_size = params['selected_size'].map(&:to_s)
	product_quantity = params['product_quantity'].map(&:to_i)
i= 1
	
	@cart = current_cart
	 
	 if params[:check] == '1' 

		 while i < selected_colorname.size
		 
		 @select = Select.new
		 @select.cart_id = @cart.id
     product = Product.find(params[:product_id])
     @select.product_id = product.id
     @select.selected_color = selected_color
     @select.product_quantity = product_quantity[i]
     @select.selected_colorname = selected_colorname[i]
     @select.selected_size = selected_size[i]
     @select.s_price = product.p_price
     @select.user_id = current_user.id
     @select.save
		 i += 1 
		 end

    respond_to do |format|
   		if selected_size.size != 1 && selected_colorname.size != 1     
				format.html { redirect_to @cart, notice: 'Select was successfully created.' }
        format.json { render :show, status: :created, location: @select }
		 else
			 format.html { 
				 flash[:size] = '사이즈와 색상을 선택해주세요.' 
				 redirect_to :back 
				}
			 format.json { render json: @select.errors, status: :unprocessable_entity }
		 end
    end
		
	elsif params[:check] == '2'
   
	  while i < selected_colorname.size

		 @select = Select.new
     @select.cart_id = @cart.id
     product = Product.find(params[:product_id])
     @select.product_id = product.id
     @select.selected_color = selected_color
     @select.product_quantity = product_quantity[i]
     @select.selected_colorname = selected_colorname[i]
     @select.selected_size = selected_size[i]
     @select.s_price = product.p_price
     @select.save
     i += 1
     end

    respond_to do |format|
        if selected_size.size != 1 && selected_colorname.size != 1
				format.html { redirect_to @cart, notice: 'Select was successfully created.' }
        format.json { render :show, status: :created, location: @select }
     else
       format.html {
         flash[:size] = '사이즈와 색상을 선택해주세요.'
         redirect_to :back
        }
       format.json { render json: @select.errors, status: :unprocessable_entity }
     end
    end

	elsif params[:check] == '3'

  while i < selected_colorname.size

     @select = Select.new
     product = Product.find(params[:product_id])
     @select.product_id = product.id
     @select.selected_color = selected_color
     @select.product_quantity = product_quantity[i]
     @select.selected_colorname = selected_colorname[i]
     @select.selected_size = selected_size[i]
     @select.s_price = product.p_price
     @select.check = params[:check]
     @select.save
		 session[:size] = selected_colorname.size
		 session[:select_id] = @select.id
		 session[:product_id] = @select.product_id
		 session[:created_at] = Time.now
     i += 1
     end

     respond_to do |format|
      if   selected_size.size != 1 && selected_colorname.size != 1
        format.html { redirect_to '/nonmember_direct_orders', notice: 'Select was successfully created.' }
        format.json { render :show, status: :created, location: @select }
      else
       format.html {
         flash[:size] = '사이즈와 색상을 선택해주세요.'
         redirect_to :back
        }
       format.json { render json: @select.errors, status: :unprocessable_entity }
      end
     end

	  
	 else

		 while i < selected_colorname.size
		 
		 @select = Select.new
		 product = Product.find(params[:product_id])
		 @select.product_id = product.id
		 @select.selected_color = selected_color
		 @select.product_quantity = product_quantity[i]
		 @select.selected_colorname = selected_colorname[i]
     @select.selected_size = selected_size[i]
     @select.s_price = product.p_price 
		 @select.user_id = current_user.id
		 @select.check = params[:check]
		 @select.save
		 session[:size] = selected_colorname.size
     session[:select_id] = @select.id
		 session[:product_id] = @select.product_id
		 i += 1
		 end

     respond_to do |format|
      if  selected_size.size != 1 && selected_colorname.size != 1
        format.html { redirect_to '/direct_orders/new', notice: 'Select was successfully created.' }
        format.json { render :show, status: :created, location: @select }
      else
       format.html {
         flash[:size] = '사이즈와 색상을 선택해주세요.'
         redirect_to :back
        }
       format.json { render json: @select.errors, status: :unprocessable_entity }
		  end
		 end
    end
	end

  # PATCH/PUT /selects/1
  # PATCH/PUT /selects/1.json
  def update
	 product = @select.product
    respond_to do |format|
      if @select.update(select_params)
				@select.s_price = product.p_price.to_i * @select.product_quantity
				@select.save
        format.html { redirect_to :back, notice: 'Select was successfully updated.' }
        format.json { render :show, status: :ok, location: @select }
      else
        format.html { 
					flash[:quantity] = '수량을 확인하세요'
					redirect_to :back
				}
        format.json { render json: @select.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /selects/1
  # DELETE /selects/1.json
  def destroy
    @select.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Select was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_select
      @select = Select.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def select_params
		 params.require(:select).permit(:product_id, :user_id, :selected_size, :selected_colorname, :s_price, :product_quantity)
		end
end
