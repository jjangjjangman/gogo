class RegistrationsController < Devise::RegistrationsController
  def new
	Address.new
  build_resource({})
  set_minimum_password_length
  yield resource if block_given?
  respond_with self.resource
  end 
 	
	def create
    build_resource(sign_up_params)
    resource.save
    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_flashing_format?
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
				@user.addresses.build
				@user.sms = params[:sms] if params[:sms]
        @user.save
				point = Point.new(name: '신규회원가입',amount: '2000', method: 'add', user_id: @user.id)
				point.save
      else
				set_flash_message :notice, :signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
			respond_to do |format|
 				format.html {
          flash[:review] = '필수 사항을 모두 입력해주세요!'
          respond_with resource
          }
			end
    end
  end

	def edit
    if params[:option]
    else
       redirect_to '/mypage/user_information'
    end
  end

  def update
    self.resource = resource_class.to_adapter.get!(send(:current_user).to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)
	  password = params[:user][:password] 
    resource_updated = update_resource(resource, account_update_params)
    yield resource if block_given?
    if resource_updated
      if is_flashing_format?
      flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
        :update_needs_confirmation : :updated
      set_flash_message :notice, flash_key
      end

			if params[:user][:password] != params[:user][:password_confirmation]
      flash[:review] = "비밀번호를 확인해주세요"
			redirect_to :back
			return
			elsif  password.length < 8 && password.length > 0
			flash[:review] = "비밀번호의 길이가 짧습니다 "
			redirect_to :back
			return
			else
		  sign_in resource_name, resource, bypass: true
	    respond_with resource, location: after_update_path_for(resource)
			end
    else
    clean_up_passwords resource
    respond_to do |format|
       format.html {
         flash[:review] = '입력하신 사항을 다시 확인해주세요!'
         redirect_to :back
				 return
         }
     end
		end
	   id = params[:id]
        # there may be a better way of doing this, devise should be able to give us these messages
    if @user.reset_password(params[:user][:password],params[:user][:password_confirmation])
       @user.save
    end	
  end
protected
	def sign_up(resource_name, resource)
	  sign_in(resource_name, resource)
	end
  def update_resource(resource, params)
    resource.update_without_password(params)
  end	

private
	def user_params
	    params.require(:user).permit(:sms, :name,:email, :password, :password_confirmation, :phone_number, :birth_year, :birth_month, :birth_day, :postcode, :address, :address2, :postcode2, :address3, :address4, addresses_attributes:[:postcode,:address, :address2])
	end
end
