class DirectOrdersController < ApplicationController
  before_action :set_direct_order, only: [:show, :edit, :update, :destroy]
	before_action :authenticate, except: [:nonmember]

  
	# GET /direct_orders
  # GET /direct_orders.json
  def index
    @direct_orders = DirectOrder.all
  end

  # GET /direct_orders/1
  # GET /direct_orders/1.json
  def show
  end

  # GET /direct_orders/new
  def new
		@select = Select.where(:id=> (session[:select_id]-session[:size]+2)..(session[:select_id]),:product_id => session[:product_id], 
																:user_id => current_user.id, :check => 0)
		@select.each do |select|
			@product_id = select.product.id
	  end
		@product = Product.find(session[:product_id])
		@user = current_user
  end

	def nonmember
		 @select = Select.where(:product_id => session[:product_id], :id=> (session[:select_id]-session[:size]+2)..(session[:select_id]),
		 												:check => 3)
    @select.each do |select|
      @product_id = select.product.id
    end
    @product = Product.find(session[:product_id])	
	end

  # GET /direct_orders/1/edit
  def edit
  end
	
	def complete
  end
  # POST /direct_orders
  # POST /direct_orders.json
  def create
    @direct_order = DirectOrder.new(direct_order_params)

    respond_to do |format|
      if @direct_order.save
        format.html { redirect_to @direct_order, notice: 'Direct order was successfully created.' }
        format.json { render :show, status: :created, location: @direct_order }
      else
        format.html { render :new }
        format.json { render json: @direct_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /direct_orders/1
  # PATCH/PUT /direct_orders/1.json
  def update
    respond_to do |format|
      if @direct_order.update(direct_order_params)
        format.html { redirect_to @direct_order, notice: 'Direct order was successfully updated.' }
        format.json { render :show, status: :ok, location: @direct_order }
      else
        format.html { render :edit }
        format.json { render json: @direct_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /direct_orders/1
  # DELETE /direct_orders/1.json
  def destroy
    @direct_order.destroy
    respond_to do |format|
      format.html { redirect_to direct_orders_url, notice: 'Direct order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_direct_order
      @direct_order = DirectOrder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def direct_order_params
      params.fetch(:direct_order, {})
    end
end
