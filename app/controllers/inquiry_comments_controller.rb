class InquiryCommentsController < ApplicationController
  before_action :set_inquiry_comment, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_admin1
	before_action :authenticate_admin


  # GET /inquiry_comments
  # GET /inquiry_comments.json
  def index
    @inquiry_comments = InquiryComment.all
  end

  # GET /inquiry_comments/1
  # GET /inquiry_comments/1.json
  def show
  end

  # GET /inquiry_comments/new
  def new
    @inquiry_comment = InquiryComment.new
		@inquiry = Inquiry.find(params[:inquiry_id])
	  session[:inquiry_id] = params[:inquiry_id]
	  @user = current_user
  end

  # GET /inquiry_comments/1/edit
  def edit
  end

  # POST /inquiry_comments
  # POST /inquiry_comments.json
  def create
    @inquiry_comment = InquiryComment.new(inquiry_comment_params)
    @user = current_user
    @inquiry_comment.inquiry_id = session[:inquiry_id]
    @inquiry = Inquiry.find(session[:inquiry_id])

    respond_to do |format|
      if @inquiry_comment.save
        format.html { redirect_to '/admin/operation_inquiry', notice: 'Inquiry comment was successfully created.' }
        format.json { render :show, status: :created, location: @inquiry_comment }
				session[:inquiry_id] = nil
      else
        format.html { render :new }
        format.json { render json: @inquiry_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inquiry_comments/1
  # PATCH/PUT /inquiry_comments/1.json
  def update
    respond_to do |format|
      if @inquiry_comment.update(inquiry_comment_params)
        format.html { redirect_to 'admin/operation_inquiry', notice: 'Inquiry comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @inquiry_comment }
      else
        format.html { render :edit }
        format.json { render json: @inquiry_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inquiry_comments/1
  # DELETE /inquiry_comments/1.json
  def destroy
    @inquiry_comment.destroy
    respond_to do |format|
      format.html { redirect_to 'admin/operation_inquiry', notice: 'Inquiry comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inquiry_comment
      @inquiry_comment = InquiryComment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inquiry_comment_params
		 params.require(:inquiry_comment).permit(:inquiry_id, :title, :content)
    end
end
