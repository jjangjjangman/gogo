class ReviewCommentsController < ApplicationController
  before_action :set_review_comment, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin1
  before_action :authenticate_admin

  # GET /review_comments
  # GET /review_comments.json
  def index
    @review_comments = ReviewComment.all
  end

  # GET /review_comments/1
  # GET /review_comments/1.json
  def show
  end

  # GET /review_comments/new
  def new
    @review_comment = ReviewComment.new
	  @review = Review.find(params[:review_id])
	  session[:review_id] = params[:review_id]
	  @user = current_user
  end

  # GET /review_comments/1/edit
  def edit
  end

  # POST /review_comments
  # POST /review_comments.json
  def create
    @review_comment = ReviewComment.new(review_comment_params)
    @user = current_user
	  @review_comment.review_id = session[:review_id]
	  @review_comment.user_id = @user.id
	  @review = Review.find(session[:review_id])

    respond_to do |format|
      if @review_comment.save
        format.html { redirect_to admin_operation_review_path, notice: 'Review comment was successfully created.' }
        format.json { render :show, status: :created, location: @review_comment }
				session[:review_id] = nil
      else
        format.html { render :new }
        format.json { render json: @review_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /review_comments/1
  # PATCH/PUT /review_comments/1.json
  def update
	@review = @review_comment.review
	@product = @review.product
    respond_to do |format|
      if @review_comment.update(review_comment_params)
        format.html { redirect_to @product, notice: 'Review comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @review_comment }
      else
        format.html { render :edit }
        format.json { render json: @review_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /review_comments/1
  # DELETE /review_comments/1.json
  def destroy
    @review_comment.destroy
    respond_to do |format|
      format.html { redirect_to review_comments_url, notice: 'Review comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_review_comment
      @review_comment = ReviewComment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def review_comment_params
      params.require(:review_comment).permit(:review_id, :user_id, :rc_title, :rc_comment)
    end
end
