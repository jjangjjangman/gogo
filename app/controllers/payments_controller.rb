class PaymentsController < ApplicationController
before_action :set_payment, only: [:refund, :complete]
before_action :authenticate_admin1, only: [:refund]
before_action :authenticate_admin, only: [:refund]


  def new
	  unless (params['buyer_name'] || params['email'])
			redirect_to '/'
		end

		 @number = params[:number]
			if params[:data] == '3'
		  		if @number == ""
       respond_to do |format|
        format.html {
            flash[:number] = '연락처를 입력해주세요!'
            redirect_to :back
            return
            }
       		end	
			end

			 if @postcode == ""
             respond_to do |format|
           format.html {
              flash[:address] = '주소를 입력해주세요!'
              redirect_to :back
              return
              }
           end
         end

			 @postcode = params[:postcode4]
       @address = params[:address7]
       @address2 = params[:address8]

	else

		 if @number == "" && current_user.phone_number == ""
       respond_to do |format|
        format.html {
            flash[:number] = '연락처를 입력해주세요!'
            redirect_to :back
            return
            }
       end
     elsif @number == ""
      @number = current_user.phone_number
     end


			delivery = params[:delivery]
		 if delivery == "1"
       postcode = params[:postcode]
       if postcode == ""
        @postcode = current_user.postcode
        @address = current_user.address
        @address2 = current_user.address2
       else
        @postcode = params[:postcode]
        @address = params[:address]
        @address2 = params[:address2]
       end
     elsif delivery == "2"
       postcode = params[:postcode2]
       if postcode == ""
        @postcode = current_user.postcode2
        @address = current_user.address3
        @address2 = current_user.address4
       else
        @postcode = params[:postcode2]
        @address = params[:address3]
        @address2 = params[:address4]
       end
     elsif delivery == "3"
       postcode = params[:postcode3]
       if postcode == ""
        @postcode =  current_user.postcode3
        @address =  current_user.address5
        @address2 =  current_user.address6
       else
        @postcode = params[:postcode3]
        @address = params[:address5]
        @address2 = params[:address6]
       end
     else
       @postcode = params[:postcode4]
       @address = params[:address7]
       @address2 = params[:address8]
     end

     if @postcode == ""
             respond_to do |format|
           format.html {
              flash[:address] = '주소를 입력해주세요!'
              redirect_to :back
              return
              }
           end
         end
	 end

			@amount = params[:amount].to_i - params[:usepoint].to_i

		if params[:request] == ""
			@request = "요청사항이 없습니다."
	 else
	    @request = params[:request]
	 end
	 @product_name = params['product_name']	 
  end

  def create

  imp_uid = params['imp_uid'] #post ajax request로부터 imp_uid확인
	parsed_imp = Iamport.payment(imp_uid).parsed_response['response']#아임포트 서버에 첫번째 접속
	imp = parsed_imp.reject{ |key, value| !Payment.attribute_names.include?(key) }

	  if parsed_imp['status'] == 'paid' or parsed_imp.nil?
		 if user_signed_in?
			 payment = current_user.payments.new(imp)
		 else
			 payment = Payment.new(imp)
		 end
		 payment.permission = true
		 payment.validation = true if parsed_imp['status'] == 'paid'
		 payment.usepoint = params[:usepoint]
		 payment.addpoint = params[:addpoint]
		 payment.request = params[:request]
		 iamport =Iamport.find(payment.merchant_uid).parsed_response['response']
		 payment.save  if payment.amount == iamport['amount']
	  elsif parsed_imp['status'] == 'ready' or parsed_imp['pay_method'] == 'vbank'
		 if user_signed_in?
			 payment = current_user.payments.new(imp)
		 else
			 payment = Payment.new(imp)
		 end
		 payment.usepoint = params[:usepoint]
		 payment.addpoint = params[:addpoint]
		 payment.request = params[:request]
     payment.save
    end

    @product_num = params['product_name']
        i = 0

	if user_signed_in?
		 while i < @product_num.size
        @select = current_user.selects.find_by_id(@product_num[i].to_i)
        @select.payment_id = payment.id
        @select.cart_id = nil
        if parsed_imp['status'] == 'paid' or parsed_imp.nil?
        @select.status = 'paid'
      elsif parsed_imp['status'] == 'ready' && parsed_imp['pay_method'] == 'vbank'
        @select.status = 'ready'
        end
        @select.save
        i += 1
      end

	 if params[:delivery] == "1" 
		current_user.postcode = params[:postcode]
		current_user.address = params[:address]
		current_user.address2 = params[:address2]
	 elsif params[:delivery] == "2"
	  current_user.address3 = params[:address]
		current_user.address4 = params[:address2]
		current_user.postcode2 = params[:postcode]
	 end
    current_user.address5 = params[:address]
    current_user.address6 = params[:address2]
    current_user.postcode3 = params[:postcode]


	 current_user.phone_number = params[:phone_number]
   usepoint = params[:usepoint]
    if usepoint != '0'
		 point = Point.new(name: '상품구매로 포인트 사용', amount: usepoint, method: 'use', payment_id: payment.id, user_id: current_user.id)
		 point.save
		end 

	if parsed_imp['status'] == 'paid' or parsed_imp.nil?
	 addpoint = params[:addpoint]
     if addpoint != '0'
			 point2 = Point.new(name: '상품구매로 포인트 적립', amount: addpoint, method: 'add', payment_id: payment.id, user_id: current_user.id)
		   point2.save
		 end
		current_user.amount = current_user.amount.to_i + payment.amount.to_i
	 current_user.point = current_user.point.to_i - usepoint.to_i + addpoint.to_i
	elsif parsed_imp['status'] == 'ready' && parsed_imp['pay_method'] == 'vbank'
		current_user.point = current_user.point.to_i - usepoint.to_i
	  end
	   current_user.save
	
	else
	 while i < @product_num.size
        @select = Select.find_by_id(@product_num[i].to_i)
        @select.payment_id = payment.id
        @select.cart_id = nil
        if parsed_imp['status'] == 'paid' or parsed_imp.nil?
        @select.status = 'paid'
      elsif parsed_imp['status'] == 'ready' && parsed_imp['pay_method'] == 'vbank'
        @select.status = 'ready'
        end
        @select.save
        i += 1
      end
	end
		
  end

	def refund
	 redirect_to :back if @payment.status == 'refunded'
	 body = {
		 imp_uid: @payment.imp_uid,
		 merchant_uid: @payment.merchant_uid,
		 amount: @payment.amount
	 }
	 if Iamport.cancel(body)
		 @payment.validation ^= true
		 @payment.status = 'refunded'
		  if @payment.cancelrq == 'request'
		   @payment.cancelrq = 'complete'
		  end
		  if @payment.save #환불이 성공한 뒤
				 user =@payment.user
				 @payment.points.each do |point|
			  if point.method == 'use'
					if point.amount != '0'
					pointnew = Point.new(name: "환불로 포인트 반환", method: "add", amount: point.amount, payment_id: @payment.id, user_id: user.id)
					pointnew.save
					user.point = user.point.to_i + pointnew.amount.to_i
					user.save
					end
				else
					if point.amount != '0'
					pointnew = Point.new(name: "환불로 포인트 회수", method: "use", amount: point.amount, payment_id: @payment.id, user_id: user.id)
					pointnew.save
					user.point = user.point.to_i - pointnew.amount.to_i
					user.save
					end
				end
			end
		  redirect_to '/admin/payments'
			end
	 else
		 redirect_to '/' #환불이 실패한 뒤 (여기서 이후에 고객관리에 대한 에러처리를 하면된다. 왜 안되었는지 
	 end
  end

	def complete
	@selects = @payment.selects
	end

	private

	def set_payment
	 @payment = Payment.find_by_imp_uid(params[:id]) #hidden value로 값 넘기기 (환불 클릭시)
	end
 

end
