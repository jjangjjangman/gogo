class HomeController < ApplicationController
  def index
	    @product_true = Product.where(status: true).order('created_at desc')
	    @product_false = Product.where(status: false).order('created_at desc')
	    @product=@product_true + @product_false
  end
	
	def happy
		 @faq = Faq.all.order('created_at desc')
	end

	def refund
	end

	def shopping
	end

	def FAQ
		 @faq = Faq.all.order('created_at desc')
	end

	def compare
	end
	
  def category
	current_cart
		@popup = Popup.first
	   product_true1 = Product.where(p_category: 'denims', status: true).order('created_at desc')
	   product_false1 = Product.where(p_category: 'denims', status: false).order('created_at desc')
	   product_denim = product_true1 + product_false1
	  @product_denims = product_denim[0..7]
	   
	   product_true2 = Product.where(p_category: 'slacks', status: true).order('created_at desc')
	   product_false2 = Product.where(p_category: 'slacks', status: false).order('created_at desc')
	   product_slack = product_true2 + product_false2
	  @product_slacks = product_slack[0..7]
	   
	   product_true3 = Product.where(p_category: 'shorts', status: true).order('created_at desc')
	   product_false3 = Product.where(p_category: 'shorts', status: false).order('created_at desc')
	   product_short = product_true3 + product_false3
	  @product_shorts = product_short[0..7]
	   
	   product_true4 = Product.where(p_category: 'pants', status: true).order('created_at desc')
	   product_false4 = Product.where(p_category: 'pants', status: false).order('created_at desc')
	   product_pant = product_true4 + product_false4
	  @product_pants = product_pant[0..7]
  end 
end
