json.extract! popup, :id, :image, :status, :width, :height, :created_at, :updated_at
json.url popup_url(popup, format: :json)
