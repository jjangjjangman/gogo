json.extract! product, :id, :p_title, :p_price, :p_comment, :m_image, :p_category, :size_SS, :size_S, :size_M, :size_L, :size_XL, :size_XXL, :created_at, :updated_at
json.url product_url(product, format: :json)
