json.extract! review, :id, :m_review_id, :product_id, :fit, :span, :length, :height, :weight, :user_size, :buy_size, :comment, :add_comment, :created_at, :updated_at
json.url review_url(review, format: :json)
