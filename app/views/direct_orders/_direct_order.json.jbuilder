json.extract! direct_order, :id, :created_at, :updated_at
json.url direct_order_url(direct_order, format: :json)
