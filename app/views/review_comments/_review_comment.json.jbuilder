json.extract! review_comment, :id, :review_id, :user_id, :rc_title, :rc_comment, :created_at, :updated_at
json.url review_comment_url(review_comment, format: :json)
