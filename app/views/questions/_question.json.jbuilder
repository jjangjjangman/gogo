json.extract! question, :id, :user_id, :product_id, :q_title, :comment, :created_at, :updated_at
json.url question_url(question, format: :json)
