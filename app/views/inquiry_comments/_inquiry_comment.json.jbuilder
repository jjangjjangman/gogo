json.extract! inquiry_comment, :id, :created_at, :updated_at
json.url inquiry_comment_url(inquiry_comment, format: :json)
