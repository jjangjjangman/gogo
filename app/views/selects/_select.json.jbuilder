json.extract! select, :id, :created_at, :updated_at
json.url select_url(select, format: :json)
