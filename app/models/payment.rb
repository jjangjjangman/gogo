class Payment < ApplicationRecord

  belongs_to :user
	has_many :refunds
	has_many :exchange
	has_many :products
	has_many :selects
	has_one :tran
	has_many :points
	
	
 def self.to_csv(options = {})
    CSV.generate(options) do |csv|
   csv << column_names
   all.each do |payment|
      csv << payment.attributes.values_at(*column_names)
    end
    end
  end

	
 def self.search(merchant_uid)
  if merchant_uid
    where('merchant_uid LIKE :search OR status LIKE :search OR buyer_tel LIKE :search', search: "%#{merchant_uid}%")
  else
    all
  end
 end

 def self.search_non(merchant_uid, buyer_tel, buyer_name)
	 if merchant_uid && buyer_tel && buyer_name
    where('merchant_uid LIKE :search AND buyer_tel LIKE :search2 AND buyer_name LIKE :search3', search: "%#{merchant_uid}%", search2: "%#{buyer_tel}%", search3: "%#{buyer_name}%")
  else
    nil
  end
 end
end
