class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable #:confirmable
  validates :name, :presence =>true

	has_many :points
    has_one :cart
	has_many :exchanges
	has_many :refunds
	has_many :selects
	has_many :payments
	has_many :addresses, dependent: :destroy
	has_many :direct_orders, dependent: :destroy
	has_many :orders, dependent: :destroy
	has_many :reviews,  dependent: :destroy
	has_many :review_comments, dependent: :destroy
	has_many :questions, dependent: :destroy
	has_many :question_comments, dependent: :destroy
	has_many :inquiries, dependent: :destroy
	accepts_nested_attributes_for :addresses
	
 def self.to_csv(options = {})
    CSV.generate(options) do |csv|
	 csv << column_names
	 all.each do |user|
	    csv << user.attributes.values_at(*column_names)
	  end
    end
   end
	
 def self.search(name)
  if name
    where('name LIKE :search OR email LIKE :search OR phone_number LIKE :search', search:  "%#{name}%")
  else
    all
  end
 end
end
