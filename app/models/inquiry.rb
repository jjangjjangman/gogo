class Inquiry < ApplicationRecord
	belongs_to :user

	has_one :inquiry_comment, dependent: :destroy
	validates :title, :presence =>true
	validates :content, :presence =>true
end
