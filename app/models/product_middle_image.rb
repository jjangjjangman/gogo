class ProductMiddleImage < ApplicationRecord
  belongs_to :product
  attachment :file
end
