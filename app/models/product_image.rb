class ProductImage < ApplicationRecord
  belongs_to :product
	attachment :file
end
