class Select < ApplicationRecord
	belongs_to :product
	belongs_to :cart
	belongs_to :user
	belongs_to :direct_order
	belongs_to :payment
	belongs_to :exchange
	belongs_to :refund

	has_one :select

  validates_numericality_of :product_quantity, :only_integer => true, :greater_than => 0, :less_than => 501
  validates :selected_size, :presence => true
	validates :selected_colorname, :presence => true
end
