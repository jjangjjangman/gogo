class Review < ApplicationRecord
  belongs_to :user
  belongs_to :product
	belongs_to :select

  validates :height, :presence =>true
	validates :weight, :presence =>true
  validates :fit, :presence =>true	
	validates :span, :presence =>true
	validates :length, :presence =>true
	validates :user_size, :presence =>true
  validates :buy_size, :presence =>true
	validates :comment, :presence =>true


	has_one :review_comment, dependent: :destroy
	has_many :review_images, dependent: :destroy
	accepts_attachments_for :review_images, attachment: :file

	mount_uploader :image, ReviewImageUploader
	
end
