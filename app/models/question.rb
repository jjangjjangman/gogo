class Question < ApplicationRecord
  belongs_to :user
  belongs_to :product
	
	validates :q_title, :presence =>true
	validates :comment, :presence =>true

	has_one :question_comment, dependent: :destroy
  has_many :question_images, dependent: :destroy
  accepts_attachments_for :question_images, attachment: :file

	mount_uploader :image, QuestionImageUploader

end
