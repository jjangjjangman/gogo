class Product < ApplicationRecord
	has_many :product_images, dependent: :destroy
	accepts_attachments_for :product_images, attachment: :file
  has_many :product_middle_images, dependent: :destroy
	accepts_attachments_for :product_middle_images, attachment: :file

	mount_uploader :m_image, ProductUploader

	mount_uploader :image1, ProductMiddleImageUploader
	mount_uploader :image2, ProductMiddleImageUploader
	mount_uploader :image3, ProductMiddleImageUploader
	mount_uploader :image4, ProductMiddleImageUploader
	mount_uploader :image5, ProductMiddleImageUploader

	has_many :selects, dependent: :destroy
	has_many :reviews, dependent: :destroy
	has_many :questions, dependent: :destroy
  has_many :payments
	before_destroy :ensure_not_referenced_by_any_select

	private
	def ensure_not_referenced_by_any_select
		if selects.empty?
			return true;
		else
			errors.add(:base, '해당 상품이 이미 존재합니다.')
			return false;
		end
	end

	 def self.to_csv(options = {})
    CSV.generate(options) do |csv|
   csv << column_names
   all.each do |product|
      csv << product.attributes.values_at(*column_names)
    end
    end
  end


 def self.search(search)
  if search
    where('p_title LIKE :search OR p_category LIKE :search OR p_price LIKE :search', search: "%#{search}%")
  else
    all
  end
 end

end
